async function getPublicGistsCount() {
  let pageCount = 1;
  let perPage = 100;
  let gistsCount = 0;
  let hasMorePages = true;

  while (hasMorePages) {
    const response = await fetch(
      `https://api.github.com/gists/public?page=${pageCount}&per_page=${perPage}`
    );

    if (!response.ok) {
      console.error(`Error: ${response.status} - ${response.statusText}`);
      return;
    }

    const gists = await response.json();

    if (gists.length === perPage) {
      gistsCount += gists.length;
      pageCount++;
    } else {
      gistsCount += gists.length;
      hasMorePages = false;
    }
  }

  return gistsCount;
}

getPublicGistsCount()
  .then((count) => console.log(`Total public gists: ${count}`))
  .catch((error) => console.error(error));
