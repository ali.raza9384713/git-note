import React from "react";
import LogoImg from "./../../assets/imgs/logo.png";

import { HeaderProps } from "./Header.interface";
import { Avatar, Button, Image, Input, Layout, Space, Dropdown } from "antd";
import { Container } from "../Container";
import { Link } from "react-router-dom";
import { UserContextObject } from "../../context/UserContext";
import { useNavigate } from "react-router-dom";
import { debounce } from "lodash";
import { paths } from "./../../constants/paths";
import { GistContextObject } from "../../context/GistContext";
import { StyledHeader } from "./Header.styled";

const { Search } = Input;

export const Header: React.FC<HeaderProps> = () => {
  const { user, logOut, token } = React.useContext(UserContextObject);
  const { paginationState, setPaginationState } =
    React.useContext(GistContextObject);
  const navigate = useNavigate();

  const debouncedSearchGists = debounce(setPaginationState, 300);

  const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    if (value) debouncedSearchGists({ ...paginationState, query: value });
  };

  const items = [
    {
      key: "0",
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          onClick={() => navigate("/gist-editor")}
        >
          Create Gist
        </a>
      ),
    },
    {
      key: "1",
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          onClick={() => {
            navigate(paths.URL_USER_PROFILE);
          }}
        >
          View profile
        </a>
      ),
    },
    {
      key: "2",
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          onClick={() => {
            logOut();
          }}
        >
          Logout
        </a>
      ),
    },
  ];

  return (
    <>
      <StyledHeader>
        <Container>
          <Link to={paths.URL_HOME}>
            <Image className="logo" src={LogoImg} width={150} preview={false} />
          </Link>
          <Space>
            <Search
              placeholder="Search Notes..."
              allowClear
              loading={false}
              onChange={onSearchChange}
            />
            {user && token ? (
              <>
                <Dropdown
                  menu={{ items }}
                  placement="bottomRight"
                  className="cursor-pointer"
                >
                  <Avatar src={user.avatar_url} />
                </Dropdown>
              </>
            ) : (
              <Link to={paths.URL_LOGIN}>
                <Button type="primary">Login</Button>
              </Link>
            )}
          </Space>
        </Container>
      </StyledHeader>
    </>
  );
};
