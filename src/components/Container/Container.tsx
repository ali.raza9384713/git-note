import React from "react";

import styled from "styled-components";
import { ContainerProps } from "./Container.interface";

export const StyledContainer = styled.div`
  max-width: 100%;
  margin: 0 auto;
  padding: 0 1rem;

  ${({ theme }) => theme.screen === "xs" && `width: 310px;`}
  ${({ theme }) => theme.screen === "sm" && `width: 700px;`}
  ${({ theme }) => theme.screen === "md" && `width: 730px;`}
  ${({ theme }) => theme.screen === "lg" && `width: 1040px;`}
  ${({ theme }) => theme.screen === "xl" && `width: 1300px;`}
  ${({ theme }) => theme.screen === "2xl" && `width: 1340px;`}
`;

export const Container: React.FC<ContainerProps> = ({ children, style }) => {
  return (
    <StyledContainer style={style} className={"container"}>
      {children}
    </StyledContainer>
  );
};
