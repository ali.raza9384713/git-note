import React from "react";

export interface ListRenderProps<T> {
  items: T[];
  ItemComponent: React.FC<any>;
  uniqueKeyName: string | number;
}

export const ListRender = <T extends {}>({
  items,
  ItemComponent,
  uniqueKeyName,
}: ListRenderProps<T>) => {
  return (
    <>
      {items.map((item, index: number) => (
        <ItemComponent key={uniqueKeyName || index} {...{ item }} />
      ))}
    </>
  );
};
