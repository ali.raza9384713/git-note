export interface ListRenderProps<T> {
  items: T[];
  resourceName: string;
  ItemComponent: React.FC<{ item: T }>;
  uniqueKeyName: string | number;
}
