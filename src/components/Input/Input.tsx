import React from "react";
import { InputProps as InputComponentProps } from "./Input.interface";

import styled from "styled-components";
import { Input as AntdInput } from "antd";
import { InputProps } from "antd/lib/input";
import { Controller } from "react-hook-form";
import { ErrorMessage, StyledInput } from "./Input.styled";

export const Input: React.FC<InputComponentProps> = ({
  name,
  control,
  rules,
  placeholder,
  ...inputProps
}) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field, fieldState }) => (
        <>
          <StyledInput {...inputProps} {...field} placeholder={placeholder} />
          {fieldState.error && (
            <ErrorMessage>{fieldState.error.message}</ErrorMessage>
          )}
        </>
      )}
    />
  );
};
